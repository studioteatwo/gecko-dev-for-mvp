/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/* eslint-env webextensions */

import { shouldInject } from "../shared/shouldInject";
import { log } from "../shared/logger";
import { WindowSSI } from "../window.ssi.type";
import { nostr, init as nostrInit } from "./nostr";
import { _invoke, addEventListener, removeEventListener } from "./api";

log("content-script working", browser.runtime.getURL("contents.bundle.js"));

// Object shared with inpage scripts.
const windowSSI = new window.Object() as WindowSSI;
windowSSI._scope = "ssi";

windowSSI.nostr = nostr;

windowSSI._proxy = new window.EventTarget();
// TODO(ssb): Ideally should conceal
windowSSI._invoke = exportFunction(_invoke(windowSSI._proxy), window);
windowSSI.addEventListener = exportFunction(
  addEventListener(windowSSI._proxy),
  window
);
windowSSI.removeEventListener = exportFunction(
  removeEventListener(windowSSI._proxy),
  window
);

if (shouldInject()) {
  // It envisions browser-native API, so the object is persisted.
  window.wrappedJSObject.ssi = windowSSI;
  for (const api of [
    window.wrappedJSObject.ssi,
    window.wrappedJSObject.ssi.nostr,
  ]) {
    for (const property of Object.getOwnPropertyNames(api)) {
      Object.defineProperty(window.wrappedJSObject.ssi.nostr, property, {
        writable: false,
        configurable: false,
      });
    }
  }
  Object.defineProperty(window.wrappedJSObject, "ssi", {
    writable: false,
    configurable: false,
  });
  XPCNativeWrapper(window.wrappedJSObject.ssi);

  nostrInit();
}
